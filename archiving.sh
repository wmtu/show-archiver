############################################################
##                                                        ##
##    WMTU Technology - WMTU 91.9FM - https://wmtu.fm/    ##
##    Script for archiving the radio stream broadcast     ##
##    Ported from the original PHP script by Brady        ##
##                                                        ##
##    Uses ffmpeg for all the fun stuff, run using a      ##
##    cron job every hour!                                ##
##                                                        ##
############################################################

#!/bin/bash

###############################################
## variables used for the rest of the script ##
###############################################

# rtp audio source settings
rtp_source="239.192.0.5" # this is a multicast IP used by axia
rtp_channel="5004"

# recording settings
current_br="320K"
past_vbr="0" # this is the lame mp3 encoder's vbr quality setting

# base location for the archives
archiveroot="/var/www/archive"

# directories for specified recordings
current="Current Week"
past="Past Shows"
special="Events"

# the times that we should record in
# hours are in in military time
times=(0 1 3 5 7 8 10 12 14 16 18 20 22)

################################################
## everything after this is the actual script ##
################################################

# get dates and times for use throughout
year=$(date -d 'now + 1 hour' +%Y)
prevYear=$(date -d 'now + 1 hour - 1 year' +%Y)
month=$(date -d 'now + 1 hour' +%B)
prevMonth=$(date -d 'now + 1 hour - 1 month' +%B)
numericDay=$(date -d 'now + 1 hour' +%d)
day=$(date -d 'now + 1 hour' +%A)
hour=$(date -d 'now + 1 hour' +%H)
showtime=$(date -d 'now + 1 hour' +%I%p)
filename="WMTU $(date -d 'now + 1 hour' +%Y.%m.%d\ %I%p).mp3"

# set the correct record duration
# some hours only record for 1 hour, most record for 2 hours
# the hours are in military time
if [ $hour -eq 0 ] || [ $hour -eq 7 ]
then
    duration=1
else
    duration=2
fi

# check if the hour is an hour to record in
record=0
for item in ${times[@]}
do
    [[ $item -eq $((10#$hour)) ]] && record=1
done

# if record is true then record
if [ $record -eq 1 ]; then
    # check for a directory for the current day
    if [ ! -d "$archiveroot/$current/$day" ]; then
        mkdir -p "$archiveroot/$current/$day"
    fi

    # check for a directory for past shows
    if [ ! -d "$archiveroot/$past/$year/$month/$numericDay" ]; then
        mkdir -p "$archiveroot/$past/$year/$month/$numericDay"
    fi

    # check for a file for the current showtime
    # this will remove the archive from previous dates,
    # and also an archive that might exist for the current hour

    # remove shows that are a year old
    if [ -d "$archiveroot/$past/$prevYear/$month/$numericDay" ]; then
        # remove the recording for the same time 1 year ago
        filecount=$(ls -1 "$archiveroot/$past/$prevYear/$month/$numericDay" | grep $showtime | wc -l)
        if [ $filecount -gt 0 ]; then
            rm $archiveroot/"$past"/$prevYear/$month/$numericDay/*$showtime.mp3
        fi

        # remove the directory for a month older than a year (leap years)
        if [ -d "$archiveroot/$past/$prevYear/$prevMonth" ]; then
            rm -rf "$archiveroot/$past/$prevYear/$prevMonth"
        fi

        # remove any empty directories (will delete month/year at end of year)
        rmdir --ignore-fail-on-non-empty -p "$archiveroot/$past/$prevYear/$month/$numericDay"
    fi

    # remove old files from the weekly archive
    filecount=$(ls -1 "$archiveroot/$current/$day" | grep $showtime | wc -l)
    if [ $filecount -gt 0 ]; then
        rm $archiveroot/"$current"/$day/*$showtime.mp3
    fi

    # rip our mp3 file
    # there are 2 encoding streams, 1 for weekly and another for yearly archives
    dumprtp $rtp_source $rtp_channel | ffmpeg -hide_banner -t $duration:10:00 -f s24be -ar 48k -ac 2 -i - \
        -acodec libmp3lame -f mp3 -b:a $current_br -content_type "audio/mpeg" "$archiveroot/$current/$day/$filename" \
        -acodec libmp3lame -f mp3 -q:a $past_vbr -content_type "audio/mpeg" "$archiveroot/$past/$year/$month/$numericDay/$filename"
fi

exit
