# Show Archiver

A bash script using ffmpeg to save DJ shows to a weekly archive.

## Requirements

- ffmpeg
- dvbstream (for dumprtp)

## Installation

Copy the script somewhere useful and run it using a cron job

```bash
# archiving script, runs every hour
55 * * * * screen -S archive -dma bash /opt/archiving/archiving.sh
```

## Tips

We use h5ai as a file indexer for our show archives. You can find out more here: <https://larsjung.de/h5ai/>
